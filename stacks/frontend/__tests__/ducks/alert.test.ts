import { completeSomething, reducer } from '../../src/ducks/alert';

describe('The alert reducer', () => {
  it('should clear all messages on non-alert actions', () => {
    const newState = reducer({ error: 'Arbitrary error' }, { type: 'Arbitrary action' });

    expect(newState.error).not.toBeDefined();
  });

  it('should store an error message when ORCID verification failed', () => {
    const newState = reducer(
      undefined,
      { payload: { error: new Error('Arbitrary error') }, type: 'flockademic/orcid/failVerification' },
    );

    expect(newState).toMatchSnapshot();
  });

  it('should store an error message when ORCID verification was aborted', () => {
    const newState = reducer(undefined, { type: 'flockademic/orcid/abortVerification' });

    expect(newState).toMatchSnapshot();
  });

  it('should store arbitrary messages indicating completion', () => {
    const newState = reducer(undefined, {
      payload: { successMessage: 'Arbitrary message' },
      type: 'flockademic/alert/completeSomething',
    });

    expect(newState).toMatchSnapshot();
  });

  it('should remove arbitrary messages indicating completion when they are ignored', () => {
    const newState = reducer(
      { success: 'Arbitrary message' },
      {
        payload: { successMessage: 'Arbitrary message' },
        type: 'flockademic/alert/ignoreCompleteSomething',
      },
    );

    expect(newState.success).toBeUndefined();
  });
});

describe('The completeSomething action creator', () => {
  it('should dispatch a completeSomething action immediately', () => {
    const thunk = completeSomething('Arbitrary success message');
    const mockDispatch = jest.fn();

    thunk(mockDispatch);

    expect(mockDispatch.mock.calls.length).toBe(1);
    expect(mockDispatch.mock.calls[0][0]).toMatchSnapshot();
  });

  it('should mark completeSomething messages as ignored after 5000 ms', () => {
    jest.useFakeTimers();
    const thunk = completeSomething('Arbitrary success message');
    const mockDispatch = jest.fn();

    thunk(mockDispatch);

    // Note: this has been renamed to advanceTimersByTime in Jest 22,
    //       so this call will have to be renamed when Jest is upgraded.
    jest.runTimersToTime(5000);

    expect(mockDispatch.mock.calls.length).toBe(2);
    expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
  });
});
